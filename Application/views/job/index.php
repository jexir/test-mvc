<?php

?>

<div class="content-wrapper">
<section class="content">
    <div class="row">
        <div class="col-md-12">
<div class="row">

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">JOBS</h3>


            </div>

            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>text</th>
                        <th>email</th>
                        <th>status</th>

                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    <?php
                    if (count($param['data'])!=0)
                    {
                        $say=0;
                        foreach ($param['data'] as $key=>$value) {
                            $say++; ?>
                            <tr>
                                <td><?=$say?></td>
                                <td><?=$value['job_name']?></td>
                                 <td><?=$value['job_text']?></td>
                                  <td><?=$value['job_email']?></td>
                                  <td><?=$value['job_status']==1 ? "Active" : "Passive"?></td>

                                <td><a href="<?=SITE_URL?>/job/edit/<?=$value['id']?>">Edit</a></td>
                                <td><a href="<?=SITE_URL?>/job/delete/<?=$value['id']?>">Delete</a></td>
                            </tr>

                       <?php }

                    }
                    ?>


                </table>
            </div>

        </div>

    </div>

</div>

        </div>
    </div>
</section>
</div>

