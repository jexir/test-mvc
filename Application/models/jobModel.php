<?php

class jobModel extends model
{

    public function create($job_name,$job_text,$job_email,$job_status)
    {
        $query=$this->db->prepare("insert into jobs(job_name,job_text,job_email,job_status) values (?,?,?,?)");
        $insert=$query->execute(array($job_name,$job_text,$job_email,$job_status));
        if ($insert)
        {
            return true;
        }else{
            return false;
        }
    }
    public function listview()
    {

            $query=$this->db->prepare("select * from jobs ");



        $query->execute();
       return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getData($id)
    {
        $query=$this->db->prepare("select * from jobs where id=?");
        $query->execute(array($id));
        return $query->fetch(PDO::FETCH_ASSOC);
    }
    public function updateData($id,$job_name,$job_text,$job_email,$job_status)
    {
        $query=$this->db->prepare("update jobs set job_name=?,job_text=?,job_email=?,job_status=? where id=?");
       $update=$query->execute(array($job_name,$job_text,$job_email,$job_status,$id));
       return $update;


    }
    public function deleteData($id)
    {
        $query=$this->db->prepare("delete from jobs where id=?");
        $query->execute(array($id));

    }
    public function pagination($db_name,$page_limit,$order,$sort){

        $page=$page_limit;
        $query=$this->db->prepare("select * from ".$db_name." ORDER BY ".$order." ".$sort);
        $query->execute();
        $all=$query->rowCount();
        $all_page=ceil($all/$page);
        $pages=isset($_GET['page']) ? (int)$_GET['page'] : 1;
        if ($pages<1) {$pages=1;}
        if ($pages>$all_page) {$page=$all_page;}
        $limit=($pages-1)*$page;
        $result=$this->db->prepare("select * from ".$db_name." ORDER BY ".$order ." ".$sort." limit ".$limit.",".$page);
        $result->execute();
        return $result->fetchAll(PDO::FETCH_ASSOC);

 }
  public function pages($db_name,$page_limit,$order,$sort,$pname){
      $page=$page_limit;
      $query=$this->db->prepare("select * from ".$db_name." ORDER BY ".$order." ".$sort);
      $query->execute();
      $all=$query->rowCount();
      return $all_page=ceil($all/$page);


  }


}