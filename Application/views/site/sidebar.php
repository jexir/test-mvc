<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=IMAGE_PATH?>/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?=$this->myuserinfo['name'];?></p>

            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">МЕНЮ</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Jobs</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?=SITE_URL?>/job/create"><i class="fa fa-circle-o"></i> Создать новый заказ</a></li>
                    <li><a href="<?=SITE_URL?>/job/"><i class="fa fa-circle-o"></i> Список заказов</a></li>

                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>пользователей</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?=SITE_URL?>/kullanici/create"><i class="fa fa-circle-o"></i> Создать нового пользователя</a></li>
                    <li><a href="<?=SITE_URL?>/kullanici/"><i class="fa fa-circle-o"></i>Список пользователей</a></li>


                </ul>
            </li>

            <li><a href="<?=SITE_URL?>/logout"><i class="fa fa-circle-o text-red"></i> <span>Выход</span></a></li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
