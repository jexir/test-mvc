<?php
/**
 * Created by PhpStorm.
 * User: JeXiR
 * Date: 31/03/2018
 * Time: 3:31 PM
 */
?>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Kullanıcı Listesi</h3>


                            </div>

                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>#</th>
                                        <th>Ad Soyad</th>
                                        <th>Email</th>

                                        <th>Düzenle</th>
                                        <th>Sil</th>
                                    </tr>
                                    <?php
                                    if (count($param['data'])!=0)
                                    {
                                        $say=0;
                                        foreach ($param['data'] as $key=>$value) {
                                            $say++; ?>
                                            <tr>
                                                <td><?=$say?></td>
                                                <td><?=$value['name']?></td>
                                                <td><?=$value['email']?></td>

                                                <td><a href="<?=SITE_URL?>/kullanici/edit/<?=$value['id']?>">Düzenle</a></td>
                                                <td><a href="<?=SITE_URL?>/kullanici/delete/<?=$value['id']?>">Sil</a></td>
                                            </tr>

                                        <?php }

                                    }
                                    ?>


                                </table>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>
</div>

