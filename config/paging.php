<?php
class Paging {
    #### start ####
    var $defPage = 1;

    function Calc($count, $perPage) {
        $uP = intval("perPageC");
        $this->Count = $count;
        $this->perPage = ($uP != 0) ? $uP : $perPage;;
        $this->Pages = ceil($this->Count / $this->perPage);
        // get page number
        if(!empty($_GET['page'])) {
            $this->Page = intval($_GET['page']);
            if($this->Page <= 0 || $this->Page > $this->Pages) $this->Page = 1;
        } else {
            $this->Page = 1;
        }
        $this->From = ($this->Page - 1) * $this->perPage;
    }

    function pageStyle($style) {
        $this->pageStyle = " style=\"".$style."\"";
    }


    function Pagination($pname) {
        $pagerArray = array();
        $Page = $this->Page;
        $Pages = $this->Pages;
        if($Pages > 1) {

            $pagerArray[] ='
 <div class="pagination__area clearfix"> <div class="pagination__title">Sayfalar</div>
 <div class="pagination pagination__content">
                    ';

            if($Page >= 4 && ($Page - 4) >= 1) {
                $pagerArray[]= ' <li><a href="'.$pname.'page=1" >«</a></li>';
            }
            if(($Page - 3) >= 1) {
                $pagerArray[] = ' <li><a  href="'.$pname.'page='.($Page - 3).'" >'.($Page - 3).'</a></li>';
            }
            if(($Page - 2) >= 1) {
                $pagerArray[] =  ' <li><a  href="'.$pname.'page='.($Page - 2).'" >'.($Page - 2).'</a></li>';
            }
            if(($Page - 1) >= 1) {
                $pagerArray[] =  ' <li><a  href="'.$pname.'page='.($Page - 1).'" >'.($Page - 1).'</a></li>';
            }

            $pagerArray[] = '<li class="active"><a  href="#">'.$Page.'</a></li>';
            if(($Page + 1) <= $Pages) {
                $pagerArray[] =  '<li><a  href="'.$pname.'page='.($Page + 1).'" >'.($Page + 1).'</a></li>';
            }
            if(($Page + 2) <= $Pages) {
                $pagerArray[] =  '<li><a  href="'.$pname.'page='.($Page + 2).'" >'.($Page + 2).'</a></li>';
            }
            if(($Page + 3) <= $Pages) {
                $pagerArray[] =  '<li><a  href="'.$pname.'page='.($Page + 3).'" >'.($Page + 3).'</a></li>';
            }
            if($Page < $Pages && ($Page + 4) <= $Pages) {
                $pagerArray[] =  '<li><a  href="'.$pname.'page='.$Pages.'" >»</a></li>';
            }

            $pagerArray[] = '
</div></div>
                ';


        } else {

        }


        return $pagerArray;


    }
    #### end ####
}
?>