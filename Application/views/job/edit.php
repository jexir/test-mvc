<?php

?>
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <?php
                helper::flashDataView("statu");
                ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">"<?=$param['data']['job_name'];?>" Update</h3>
                    </div>

                    <form role="form" action="<?=SITE_URL?>/job/update/<?=$param['data']['id'];?>" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" class="form-control" name="job_name" id="exampleInputEmail1" value="<?=$param['data']['job_name'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Text</label>
                                <input type="text" class="form-control" name="job_text" id="exampleInputEmail1" value="<?=$param['data']['job_text'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" name="job_email" id="exampleInputEmail1" value="<?=$param['data']['job_email'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status</label>
                                <select name="job_status" id="" class="form-control">

                                    <option value="1" <?=$param['data']['job_status']==1 ? "selected" : null?>>Active</option>
                                    <option value="0" <?=$param['data']['job_status']==0 ? "selected" : null?>>Passive</option>
                                </select>

                            </div>




                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
</div>
