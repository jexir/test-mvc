<?php


class admin extends controller
{
    public function index()
    {
         
        if (!$this->sessionManager->isLogged()){
            helper::redirect(SITE_URL."/login");
            die();
        }

        $this->render('site/header');
        $this->render('site/sidebar');
        $this->render('admin');
        $this->render('site/footer');
    }

}