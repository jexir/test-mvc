<?php


class kullanici extends controller
{
    public function index()
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
       
        $data=$this->model('uyeModel')->listview();
        $this->render('site/header');
        $this->render('site/sidebar');
        $this->render('kullanici/index',['data'=>$data]);
        $this->render('site/footer');

    }
    public function create()
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
       
        $this->render('site/header');
        $this->render('site/sidebar');
        $this->render('kullanici/create');
        $this->render('site/footer');
    }
    public function send()
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
       
        if ($_POST)
        {
            $name=helper::cleaner($_POST['name']);
            $email=helper::cleaner($_POST['email']);
            $password=helper::cleaner($_POST['password']);
            $permissions=$_POST['permissions'];

            if ($name!="" and $password!="" and $email!="")
            {
                $ekle=$this->model('uyeModel')->create($name,$email,md5($password),implode(",",$permissions));
                if ($ekle)
                {
                    helper::flashData("statu","Пользователь успешно добавлен");
                    helper::redirect(SITE_URL."/kullanici/create");

                }else{
                    helper::flashData("statu","Не удалось добавить пользователя");
                    helper::redirect(SITE_URL."/kullanici/create");
                }

            }else{
                helper::flashData("statu","Пожалуйста, введите все поля");
                helper::redirect(SITE_URL."/kullanici/create");
            }


        }else{
            exit("Yetki Yok");

        }
    }
    public function edit($id)
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
       

        $data=$this->model('uyeModel')->getData($id);
        $this->render('site/header');
        $this->render('site/sidebar');
        $this->render('kullanici/edit',['data'=>$data]);
        $this->render('site/footer');
    }

    public function update($id)
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
       
        if ($_POST)
        {
            $name=helper::cleaner($_POST['name']);
            $email=helper::cleaner($_POST['email']);
            $password=helper::cleaner($_POST['password']);
            $permissions=$_POST['permissions'];
            $data=$this->model('uyeModel')->getData($id);
            $passwordE=$data['password'];


            if ($name!="" and $email!="")
            {


                if ($password==$passwordE)
                {

                    $password=$passwordE;
                }
                else{
                    $password=md5($password);
                }
                $ekle=$this->model('uyeModel')->updateData($id,$name,$email,$password,implode(",",$permissions));
                if ($ekle)
                {
                    helper::flashData("statu","Пользователь успешно отредактирован");
                    helper::redirect(SITE_URL."/kullanici/edit/".$id);

                }else{
                    helper::flashData("statu","Пользователь не отредактирован");
                    helper::redirect(SITE_URL."/kullanici/edit/".$id);
                }

            }else{
                helper::flashData("statu","Пожалуйста, введите все поля");
                helper::redirect(SITE_URL."/kullanici/edit/".$id);
            }


        }else{
            exit("Нет авторизации");

        }
    }
    public function delete($id)
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
       
        $this->model('uyeModel')->deleteData($id);
        helper::redirect(SITE_URL."/kullanici");


    }


}