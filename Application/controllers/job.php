<?php

class job extends controller
{
    public function index()
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
        
        $data=$this->model('jobModel')->listview();
        $this->render('site/header');
        $this->render('site/sidebar');
        $this->render('job/index',['data'=>$data]);
        $this->render('site/footer');

    }
    public function create()
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
        
        $this->render('site/header');
        $this->render('site/sidebar');
        $this->render('job/create');
        $this->render('site/footer');
    }
    public function send()
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
        
        if ($_POST)
        {
           
            $job_name=helper::cleaner($_POST['job_name']);
            $job_text=helper::cleaner($_POST['job_text']);
            $job_email=helper::cleaner($_POST['job_email']);
            $job_status=helper::cleaner($_POST['job_status']);
            if ($job_name!="" && $job_text!="" && $job_email!="" && $job_status!="")
            {
                $ekle=$this->model('jobModel')->create($job_name,$job_text,$job_email,$job_status);
                if ($ekle)
                {
                    helper::flashData("statu","задача успешно добавлен");
                    helper::redirect(SITE_URL."/job/create");

                }else{
                    helper::flashData("statu","задача не добавлен");
                    helper::redirect(SITE_URL."/job/create");
                }

            }else{
                helper::flashData("statu","Пожалуйста, введите все поля");
                helper::redirect(SITE_URL."/job/create");
            }


        }else{
            exit("Нет авторизации");

        }
    }
    public function edit($id)
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
        

        $data=$this->model('jobModel')->getData($id);
        $this->render('site/header');
        $this->render('site/sidebar');
        $this->render('job/edit',['data'=>$data]);
        $this->render('site/footer');
    }

    public function update($id)
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
        
        if ($_POST)
        {

             $job_name=helper::cleaner($_POST['job_name']);
            $job_text=helper::cleaner($_POST['job_text']);
            $job_email=helper::cleaner($_POST['job_email']);
            $job_status=helper::cleaner($_POST['job_status']);
            if ($job_name!="" && $job_text!="" && $job_email!="" && $job_status!="")
            {
                $duzenle=$this->model('jobModel')->updateData($id,$job_name,$job_text,$job_email,$job_status);
                if ($duzenle)
                {
                    helper::flashData("statu","задача успешно отредактирован");
                    helper::redirect(SITE_URL."/job/edit/".$id);

                }else{
                    helper::flashData("statu","задача не отредактирован");
                    helper::redirect(SITE_URL."/job/edit/".$id);
                }

            }else{
                helper::flashData("statu","Пожалуйста, введите все поля");
                helper::redirect(SITE_URL."/job/edit/".$id);
            }


        }else{
            exit("Нет авторизации");

        }
    }
    public function delete($id)
    {
        if(!$this->sessionManager->isLogged()){helper::redirect(SITE_URL); die();}
        
        $this->model('jobModel')->deleteData($id);
        helper::redirect(SITE_URL."/job");


    }

}