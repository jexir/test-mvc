<?php

define("CONTROLLERS_PATH","Application/controllers");
define("VIEWS_PATH","Application/views");
define("MODELS_PATH","Application/models");
define("DB_HOST","localhost");
define("DB_NAME","foxkievc_mvc");
define("DB_USERNAME","foxkievc_mvc");
define("DB_PASSWORD","foxkievc_mvc");
define("SITE_URL","http://test.foxkiev.com");

define("CSS_PATH",SITE_URL."/public/css");
define("JS_PATH",SITE_URL."/public/js");
define("IMAGE_PATH",SITE_URL."/public/img");
define("BOWER_PATH",SITE_URL."/public/bower_components");
define("PLUGIN_PATH",SITE_URL."/public/plugins");

