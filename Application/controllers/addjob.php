<?php

class addjob extends controller
{
    public function index()
    {



        $this->render('addjob');


    }
    public function create()
    {

        $this->render('addjob');

    }
    public function send()
    {

        
        if ($_POST)
        {
           
            $job_name=helper::cleaner($_POST['job_name']);
            $job_text=helper::cleaner($_POST['job_text']);
            $job_email=helper::cleaner($_POST['job_email']);
            $job_status=helper::cleaner($_POST['job_status']);

            if ($job_name!="" && $job_text!="" && $job_email!="" && $job_status!="") {
                if (!filter_var($job_email, FILTER_VALIDATE_EMAIL)) {
                    helper::flashData("statu", "Ошибка! Пожалуйста, введите корректные электронную почту");
                    helper::redirect(SITE_URL);
                }else{
                $ekle = $this->model('jobModel')->create($job_name, $job_text, $job_email, $job_status);
                if ($ekle) {
                    helper::flashData("statu", "задача успешно добавлен");
                    helper::redirect(SITE_URL);

                } else {
                    helper::flashData("statu", "задача не добавлен");
                    helper::redirect(SITE_URL);
                }
            }

            }else{
                helper::flashData("statu","Пожалуйста, введите все поля");
                helper::redirect(SITE_URL);
            }



        }else{
            exit("Нет авторизации");

        }
    }



}