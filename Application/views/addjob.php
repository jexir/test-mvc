<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Jobs</title>
</head>
<body>
	<hr>
	<div class="container">
  <div class="row">
<div class="col-md-6 offset-md-3">
	

	<a href="/admin" class="float-right btn btn-info">LOGIN</a>
	
</div>
</div>
</div>
<hr>
    <?php if (helper::flashDataView("statu")) { ?>
    <div class="container">
        <div class="row">
        <div class="alert alert-primary" role="alert">
            <?php helper::flashDataView("statu"); ?>
        </div>
        </div>
    </div>
    <?php } ?>
    
<div class="container">
  <div class="row">

      <form role="form" action="<?=SITE_URL?>/addjob/send" method="POST">
          <div class="box-body">
              <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" name="job_name" id="exampleInputEmail1">
              </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">Text</label>
                  <input type="text" class="form-control" name="job_text" id="exampleInputEmail1">
              </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" class="form-control" name="job_email" id="exampleInputEmail1">
              </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">Status</label>
                  <select name="job_status" id="" class="form-control">

                      <option value="1">Active</option>
                      <option value="0">Passive</option>
                  </select>

              </div>



          </div>
          <!-- /.box-body -->

          <div class="box-footer">
              <button type="submit" class="btn btn-primary">Save</button>
          </div>
      </form>
 
</div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>