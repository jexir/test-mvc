<?php

class login extends controller
{
    public function index()
    {
        $this->render('login');
    }
    public function send()
    {
        if ($_POST)
        {
            $name=helper::cleaner($_POST['name']);
            $password=helper::cleaner($_POST['password']);
            if ($name!="" and $password!="")
            {
                $control=$this->model('uyeModel')->control($name,md5($password));

                if ($control==0)
                {
                    helper::flashData("statu","пользователь не найден");
                    helper::redirect(SITE_URL."/login");
                }else{
                    sessionManager::createSession(['name'=>$name,'password'=>$password]);
                    helper::redirect(SITE_URL."/admin");
                }


            }
            else{
                helper::flashData("statu","Пожалуйста, введите все поля");
                helper::redirect(SITE_URL."/login");
            }

        }else{
            exit("Неверный логин");
        }

    }
}