<?php

class uyeModel extends model
{
    public function control($name,$password)
    {
        $query=$this->db->prepare("select * from uyeler where name=? and password=?");
        $query->execute(array($name,$password));
        return $query->rowCount();
    }

    public function create($name,$email,$password)
    {
        $query=$this->db->prepare("insert into uyeler(name,email,password) values (?,?,?)");
        $insert=$query->execute(array($name,$email,$password));
        if ($insert)
        {
            return true;
        }else{
            return false;
        }
    }
  
    public function listview()
    {
        $query=$this->db->prepare("select * from uyeler");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getData($id)
    {
        $query=$this->db->prepare("select * from uyeler where id=?");
        $query->execute(array($id));
        return $query->fetch(PDO::FETCH_ASSOC);
    }
    public function updateData($id,$name,$email,$password)
    {
        $query=$this->db->prepare("update uyeler set name=?,email=?,password=? where id=?");
        $update=$query->execute(array($name,$email,$password,$id));
        return $update;


    }
    public function deleteData($id)
    {
        $query=$this->db->prepare("delete from uyeler where id=?");
        $query->execute(array($id));

    }
}