<?php


class main extends controller
{
    public function index()
    {
        if (isset($_GET['order'])) {
            $order=$_GET['order'];
        }else{
            $order="id";
        }

        if (isset($_GET['sort'])) {
            $sort=$_GET['sort'];
        }else{
            $sort="ASC";

        }
        $pname=isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $page_limit=3;
        $datam=$this->model('jobModel')->pagination("jobs",$page_limit,$order,$sort);
        $pages=$this->model('jobModel')->pages("jobs",$page_limit,$order,$sort,$pname);




        
        $this->render('home',['pages'=>$pages,'datam'=>$datam,'pname'=>$pname]);
        

    }
   

}