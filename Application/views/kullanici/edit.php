<?php
/**
 * Created by PhpStorm.
 * User: JeXiR
 * Date: 31/03/2018
 * Time: 2:22 PM
 */
?>
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <?php
                helper::flashDataView("statu");
                ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">"<?=$param['data']['name']?>" Düzenleniyor</h3>
                    </div>

                    <form role="form" action="<?=SITE_URL?>/kullanici/update/<?=$param['data']['id']?>" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kullanıcı Adı</label>
                                <input type="text" class="form-control" name="name" id="exampleInputEmail1" value="<?=$param['data']['name']?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kullanıcı Email</label>
                                <input type="email" class="form-control" name="email" id="exampleInputEmail1" value="<?=$param['data']['email']?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kullanıcı Şifre</label>
                                <input type="password" class="form-control" name="password" id="exampleInputEmail1" value="<?=$param['data']['password']?>">
                            </div>

                            <div class="box-body">
                                <div class="form-group">
                                    <label for="">İzinler</label>
                                    <br>
                                    <?php

                                    foreach (unserialize(PERMISSIONS) as $key => $value)
                                    {
                                        ?>
                                        <input type="checkbox" <?php
                                        if(in_array($key,explode(',',$param['data']['permissions'])))
                                        { echo 'checked'; }
                                        ?>  name="permissions[]" value="<?=$key;?>"><?=$value;?><br>
                                    <?php }
                                    ?>
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
</div>
